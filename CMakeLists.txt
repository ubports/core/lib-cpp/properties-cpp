cmake_minimum_required(VERSION 2.8)

project(properties-cpp)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

include(GNUInstallDirs)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Werror -Wall -pedantic -Wextra -fPIC -fvisibility=hidden -pthread")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Werror -Wall -fno-strict-aliasing -fvisibility=hidden -fvisibility-inlines-hidden -pedantic -Wextra -fPIC -pthread")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,--no-undefined")

set(PROPERTIES_CPP_VERSION_MAJOR 0)
set(PROPERTIES_CPP_VERSION_MINOR 0)
set(PROPERTIES_CPP_VERSION_PATCH 3)

enable_testing()

include_directories(
  include/
)

add_subdirectory(doc)
add_subdirectory(data)
add_subdirectory(include)
add_subdirectory(tests)

find_package(CoverageReport)
enable_coverage_report(
  TARGETS ${COVERAGE_TEST_TARGETS}
  TESTS ${COVERAGE_TEST_TARGETS}
  FILTER /usr/include ${CMAKE_BINARY_DIR}/*
)
